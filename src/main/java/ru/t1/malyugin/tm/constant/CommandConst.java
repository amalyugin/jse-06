package ru.t1.malyugin.tm.constant;

public final class CommandConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    public static final String EXIT = "exit";

    private CommandConst() {
    }

}

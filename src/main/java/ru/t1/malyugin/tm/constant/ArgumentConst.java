package ru.t1.malyugin.tm.constant;

public final class ArgumentConst {

    public static final String HELP = "-h";

    public static final String VERSION = "-v";

    public static final String ABOUT = "-a";

    private ArgumentConst() {
    }

}
